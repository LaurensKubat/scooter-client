package scooter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMissCache_allowed(t *testing.T) {
	m := NewMissCache()

	// First time we should pass:
	ok := m.allowed("BTC")
	assert.True(t, ok)

	// Second time we should be blocked
	ok = m.allowed("BTC")
	assert.False(t, ok)
}
