package scooter

import (
	"context"
	"github.com/jinzhu/copier"
	"sync"
	"time"

	sc "github.com/CIP-NL/miss-piggy/generated/go/scooter"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

var (
	// Host where the scooter service is permanently residing
	Host = "35.234.144.194"
	// Port where the scooter service is permanently residing
	Port = 8090
)

const (
	// Amount of times the cache can be missed before a repopulation is triggered
	CacheMissesBeforeRepopulate = 3
	Timout                      = time.Hour
)

// Cache stores ticker/exchange pairs using the service scooter, to ensure all services are using the same
// ids and tickers. Cache uses gRPC to dial scooter. On cache misses scooter is dialed for an update (which means
// the lookup time is increased from ns to tens of ms).
type Cache struct {
	client sc.ScooterClient

	assetMu sync.RWMutex
	// string is the key generated from the aliases belonging to that asset
	AssetCache map[string]*sc.Asset

	aliasMu sync.RWMutex
	// string is the key generated from combination of asset and alias using AliasKey
	AliasCache map[string]*sc.Alias

	exchMu sync.RWMutex
	// string is the key generated from enum Exchange.String()
	ExchangeCache map[string]*sc.ExchangeInfo

	misses MissCache

	ctx context.Context
}

// New connects to the service scooter and instantiates a cache.
func New(opts ...grpc.DialOption) (*Cache, error) {
	if len(opts) == 0 {
		opts = append(opts, grpc.WithInsecure())
	}

	client, err := NewRemoteClient(Host, Port, opts...)
	if err != nil {
		return nil, err
	}
	c := &Cache{
		client:        *client,
		assetMu:       sync.RWMutex{},
		AssetCache:    make(map[string]*sc.Asset),
		aliasMu:       sync.RWMutex{},
		AliasCache:    make(map[string]*sc.Alias),
		exchMu:        sync.RWMutex{},
		ExchangeCache: make(map[string]*sc.ExchangeInfo),
		misses:        NewMissCache(),
	}

	err = c.populate()
	if err != nil {
		return nil, err
	}
	return c, nil
}

// New connects to the service scooter and instantiates a cache.
func NewContext(ctx context.Context, opts ...grpc.DialOption) (*Cache, error) {
	if len(opts) == 0 {
		opts = append(opts, grpc.WithInsecure())
	}

	client, err := NewRemoteClient(Host, Port, opts...)
	if err != nil {
		return nil, err
	}
	c := &Cache{
		client:        *client,
		assetMu:       sync.RWMutex{},
		AssetCache:    make(map[string]*sc.Asset),
		aliasMu:       sync.RWMutex{},
		AliasCache:    make(map[string]*sc.Alias),
		exchMu:        sync.RWMutex{},
		ExchangeCache: make(map[string]*sc.ExchangeInfo),
		misses:        NewMissCache(),
	}
	err = c.WithCTX(ctx).populate()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Cache) WithCTX(ctx context.Context) *Cache {
	c.ctx = ctx
	return c
}

// Query checks the local cache, and if a cache miss occurs, queries scooter for an update on the asset.
// an unknown asset error means the maintainer of scooter is informed and will add the asset ASAP.
func (c *Cache) GetAsset(tkr string, exch sc.Exchange) (asset *sc.Asset, err error) {
	c.assetMu.RLock()

	if alias, ok := c.AssetCache[AliasKey(sc.Alias{Ticker: tkr, Exchange: exch})]; ok {
		c.assetMu.RUnlock()
		return alias, nil
	}

	if !c.misses.allowed(AliasKey(sc.Alias{Ticker: tkr, Exchange: exch})) {
		c.assetMu.RUnlock()
		return nil, errors.New("ratelimit reached for: " + tkr + exch.String())
	}

	// we need to unlock the read lock before population
	c.assetMu.RUnlock()

	err = c.populate()
	if err != nil {
		return nil, err
	}
	return c.GetAsset(tkr, exch)
}

// GetAlias looks up an alias belonging to an Asset
func (c *Cache) GetAlias(asset sc.Asset, exch sc.Exchange) (alias *sc.Alias, err error) {
	c.aliasMu.RLock()

	if res, ok := c.AliasCache[AssetKey(asset)+exch.String()]; ok {
		c.aliasMu.RUnlock()
		return res, nil
	}

	if !c.misses.allowed(AssetKey(asset) + exch.String()) {
		c.aliasMu.RUnlock()
		return nil, errors.New("ratelimit reached for: " + asset.Ticker + exch.String())
	}

	// we need to unlock the read lock before population
	c.aliasMu.RUnlock()

	err = c.populate()
	if err != nil {
		return nil, err
	}
	return c.GetAlias(asset, exch)
}

// Convert looks up an alias belonging to an Asset
func (c *Cache) GetExchange(exch sc.Exchange) (exchange *sc.ExchangeInfo, err error) {
	c.exchMu.RLock()
	if exchange, ok := c.ExchangeCache[exch.String()]; ok {
		c.exchMu.RUnlock()
		return exchange, nil
	}

	if !c.misses.allowed(exch.String()) {
		c.exchMu.RUnlock()
		return nil, errors.New("ratelimit reached for: " + exch.String())
	}

	// we need to unlock the read lock before population
	c.exchMu.RUnlock()

	err = c.populate()
	if err != nil {
		return nil, err
	}
	return c.GetExchange(exch)
}

func (c *Cache) AssetExists(asset sc.Asset, exchange sc.Exchange) bool {
	resp, err := c.GetAlias(asset, exchange)
	if err != nil || resp == nil {
		return false
	}
	return true
}

// Populates initializes the cache with all known assets
func (c *Cache) populate() error {
	ctx := c.getCTX()

	aliases, err := c.client.GetAliases(ctx, &empty.Empty{})
	if err != nil {
		return err
	}

	exchanges, err := c.client.GetExchanges(ctx, &empty.Empty{})
	if err != nil {
		return err
	}

	c.assetMu.Lock()
	defer c.assetMu.Unlock()

	c.aliasMu.Lock()
	defer c.aliasMu.Unlock()

	c.exchMu.Lock()
	defer c.exchMu.Unlock()

	for _, alias := range aliases.Alias {
		c.AssetCache[AliasKey(*alias)] = alias.Asset
		c.AliasCache[AssetAliasKey(*alias)] = alias
	}

	for _, exch := range exchanges.Exchanges {
		c.ExchangeCache[ExchangeKey(*exch)] = exch
	}
	return nil
}

func (c *Cache) getCTX() context.Context {
	if c.ctx == nil {
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		return ctx
	}
	ctx := context.Background()
	copier.Copy(ctx, c.ctx)
	c.ctx = nil
	return ctx
}
