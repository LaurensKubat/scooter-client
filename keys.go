package scooter

import (
	"fmt"

	sc "github.com/CIP-NL/miss-piggy/generated/go/scooter"
)

// AliasKey returns a unique key for the alias
func AliasKey(a sc.Alias) string {
	return fmt.Sprintf("%s:%s", a.Exchange.String(), a.Ticker)
}

// AssetKey returns a unique key for pointing asset to alias.
func AssetAliasKey(a sc.Alias) string {
	return AssetKey(*a.Asset) + a.Exchange.String()
}

// Key returns a unique key for the asset
func AssetKey(a sc.Asset) string {
	return a.Ticker
}

// ExchangeKey returns a unique key for the exchange
func ExchangeKey(e sc.ExchangeInfo) string {
	return e.Enum.String()
}
