package scooter

import (
	"fmt"
	"os"
	"testing"

	sc "github.com/CIP-NL/miss-piggy/generated/go/scooter"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Unit tests marked with INTEGRATION in this suite are run against a life version of scooter to ascertain compatibility.

func TestNew(t *testing.T) {
	if os.Getenv("INTEGRATION") != "true" {
		t.Skip()
	}

	c, err := New()
	require.NoError(t, err)

	assert.True(t, len(c.AssetCache) > 4)
	assert.True(t, len(c.AliasCache) > 0)
	assert.True(t, len(c.ExchangeCache) > 4)
}

func TestCache_GetAlias(t *testing.T) {
	if os.Getenv("INTEGRATION") != "true" {
		t.Skip()
	}

	c, err := New()
	require.NoError(t, err)

	alias, err := c.GetAlias(sc.Asset{Ticker: "BTC:BITCOIN"}, sc.Exchange_Binance)
	require.NoError(t, err)
	assert.Equal(t, "BTC", alias.Ticker)
}

func TestCache_GetAsset(t *testing.T) {
	if os.Getenv("INTEGRATION") != "true" {
		t.Skip()
	}

	c, err := New()
	require.NoError(t, err)

	asset, err := c.GetAsset("BTC", sc.Exchange_Binance)
	require.NoError(t, err)
	assert.Equal(t, "BTC:BITCOIN", asset.Ticker)
}

func TestCache_GetExchange(t *testing.T) {
	if os.Getenv("INTEGRATION") != "true" {
		t.Skip()
	}

	c, err := New()
	require.NoError(t, err)

	exch, err := c.GetExchange(sc.Exchange_Binance)
	require.NoError(t, err)
	assert.Equal(t, "binance", exch.Name)
}

func TestDoesNotBlock(t *testing.T) {
	c, err := New()
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		c.GetAsset(fmt.Sprintf("%d", i), sc.Exchange_Bittrex)
	}
}
