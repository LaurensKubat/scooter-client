package scooter

import (
	"fmt"

	sc "github.com/CIP-NL/miss-piggy/generated/go/scooter"
	"google.golang.org/grpc"
)

func NewRemoteClient(host string, port int, opts ...grpc.DialOption) (*sc.ScooterClient, error) {
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", host, port), opts...)
	if err != nil {
		return nil, err
	}
	client := sc.NewScooterClient(conn)

	return &client, nil
}
