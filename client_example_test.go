package scooter

import (
	"fmt"
	s "github.com/CIP-NL/miss-piggy/generated/go/scooter"
)

func Example() {
	sc, err := New()
	if err != nil {
		panic(err)
	}

	// Query will check its own cache or send a get to scooter.
	fmt.Println(sc.GetAsset("BTC", s.Exchange_Binance))
	// Output: ID:1 Ticker:"BTC:BITCOIN" VerboseName:"bitcoin"  <nil>
}
