package scooter

import (
	"sync"
	"time"
)

type MissCache struct {
	missMu sync.RWMutex
	misses map[string]*miss
}

// miss represents a cache miss and a remote miss, so that we do not spam the remote.
type miss struct {
	amount int32
	// Time when we are allowed check scooter again.
	timout time.Time
}

func (m MissCache) allowed(key string) bool {
	m.createNewIfNotExists(key)

	if m.misses[key].amount == 0 {
		m.missMu.Lock()
		m.misses[key].amount += 1
		m.misses[key].timout = m.misses[key].timout.Add(Timout)
		m.missMu.Unlock()
		return true
	}

	if time.Now().After(m.misses[key].timout) {
		m.missMu.Lock()
		m.misses[key].amount += 1
		m.misses[key].timout = time.Now().Add(Timout)
		m.missMu.Unlock()
		return true
	}
	return false
}

func NewMissCache() MissCache {
	return MissCache{
		missMu: sync.RWMutex{},
		misses: make(map[string]*miss),
	}
}

func (m MissCache) createNewIfNotExists(key string) {
	if _, ok := m.misses[key]; !ok {
		m.misses[key] = &miss{0, time.Now()}
	}
}
